package com.davidups.beer.core.extensions

import androidx.navigation.NavOptions

fun navOptions(): NavOptions? {
    return NavOptions.Builder()
        /*.setEnterAnim(R.anim.fragment_enter)
        .setExitAnim(R.anim.fragment_exit)
        .setPopEnterAnim(R.anim.fragment_enter)
        .setPopExitAnim(R.anim.fragment_exit)*/
        .build()
}