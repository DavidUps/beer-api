package com.davidups.beer.core.di

import com.davidups.beer.features.beer.domain.repository.BeerRepository
import com.davidups.beer.features.beer.domain.repository.BeerRepositoryImp
import org.koin.dsl.module


val repositoryModule = module {

    factory<BeerRepository> { BeerRepositoryImp(get(), get()) }

}