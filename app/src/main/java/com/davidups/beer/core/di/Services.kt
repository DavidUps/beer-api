package com.davidups.beer.core.di

import com.davidups.beer.features.beer.data.services.BeerService
import org.koin.dsl.module

val dataSourceModule = module {

    factory { BeerService(get()) }
}
