package com.davidups.beer.core.di

import com.davidups.beer.features.beer.domain.usescases.GetBeers
import org.koin.dsl.module

val useCaseModule = module {

    factory { GetBeers(get()) }
}