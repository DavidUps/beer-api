package com.davidups.beer.features.beer.domain.usescases

import com.davidups.beer.core.functional.State
import com.davidups.beer.core.interactor.UseCase
import com.davidups.beer.features.beer.data.models.view.BeerView
import com.davidups.beer.features.beer.domain.repository.BeerRepository

class GetBeers(private val repository: BeerRepository) :
    UseCase<State<MutableList<BeerView>>, UseCase.None>() {

    override fun run(params: None?) = repository.getBeers()
}