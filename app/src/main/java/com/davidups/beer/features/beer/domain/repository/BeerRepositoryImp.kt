package com.davidups.beer.features.beer.domain.repository

import com.davidups.beer.core.exception.Failure
import com.davidups.beer.core.functional.Error
import com.davidups.beer.core.functional.State
import com.davidups.beer.core.functional.Success
import com.davidups.beer.core.platform.NetworkHandler
import com.davidups.beer.features.beer.data.models.view.BeerView
import com.davidups.beer.features.beer.data.services.BeerService
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

class BeerRepositoryImp(
    private val networkHandler: NetworkHandler,
    private val service: BeerService
) : BeerRepository {

    override fun getBeers() = flow {
        emit(getBeersFromApi())
    }
        .catch { emit(Error(Failure.Throwable(it))) }

    private suspend fun getBeersFromApi(): State<MutableList<BeerView>> {
        return when (networkHandler.isConnected) {
            true -> {
                service.getBeer().run {
                    if (isSuccessful && body() != null) {
                        Success(body()!!.map { it.toData().toView() }.toMutableList())
                    } else {
                        Error(Failure.ServerError(code()))
                    }
                }
            }
            false, null -> {
                Error(Failure.NetworkConnection())
            }
        }
    }
}