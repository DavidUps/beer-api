package com.davidups.beer.features.beer.views.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.davidups.beer.R
import com.davidups.beer.core.extensions.loadFromUrl
import com.davidups.beer.core.platform.BaseFragment
import com.davidups.beer.core.platform.viewBinding.viewBinding
import com.davidups.beer.databinding.FragmentBeerBinding

class BeerFragment : BaseFragment(R.layout.fragment_beer) {

    private val binding by viewBinding(FragmentBeerBinding::bind)
    private val argument by navArgs<BeerFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    private fun initView() {
        with(argument.beer) {
            binding.iv.loadFromUrl(this.image_url)
            binding.tvName.text = this.name
            binding.tvDescription.text = this.description
            binding.tvDegrees.text = "${binding.tvDegrees.text} ${this.abv}"
        }
    }
}