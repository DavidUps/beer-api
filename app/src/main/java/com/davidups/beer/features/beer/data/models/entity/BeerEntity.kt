package com.davidups.beer.features.beer.data.models.entity

import com.davidups.beer.core.extensions.empty
import com.davidups.beer.features.beer.data.models.data.Beer

data class BeerEntity(
    private val id: Int?,
    private val name: String?,
    private val description: String?,
    private val image_url: String?,
    private val abv: Double?
) {

    companion object {
        fun empty() = BeerEntity(Int.empty(), String.empty(), String.empty(), String.empty(), Double.empty())
    }

    fun toData() = Beer(id, name, description, image_url, abv)
}