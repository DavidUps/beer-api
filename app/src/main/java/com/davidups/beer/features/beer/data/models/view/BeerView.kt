package com.davidups.beer.features.beer.data.models.view

import android.os.Parcelable
import com.davidups.beer.core.extensions.empty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BeerView(
    val name: String,
    val description: String,
    val image_url: String,
    val abv: Double
): Parcelable {

    companion object {
        fun empty() = BeerView(String.empty(), String.empty(), String.empty(), Double.empty())
    }
}