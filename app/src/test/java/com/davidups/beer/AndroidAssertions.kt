package com.davidups.beer

import androidx.appcompat.app.AppCompatActivity
import kotlin.reflect.KClass
import org.amshove.kluent.shouldEqual
import org.robolectric.Robolectric
import org.robolectric.Shadows

infix fun KClass<out AppCompatActivity>.`shouldNavigateTo`(nextActivity: KClass<out AppCompatActivity>) =
    run {
        val originActivity = Robolectric.buildActivity(this.java).get()
        val shadowActivity = Shadows.shadowOf(originActivity)
        val nextIntent = shadowActivity.peekNextStartedActivity()

        nextIntent.component!!.className shouldEqual nextActivity.java.canonicalName
    }
