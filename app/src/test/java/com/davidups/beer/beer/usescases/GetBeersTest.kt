package com.davidups.beer.beer.usescases

import com.davidups.beer.UnitTest
import com.davidups.beer.core.functional.Success
import com.davidups.beer.core.platform.NetworkHandler
import com.davidups.beer.features.beer.data.models.entity.BeerEntity
import com.davidups.beer.features.beer.data.models.view.BeerView
import com.davidups.beer.features.beer.data.services.BeerApi
import com.davidups.beer.features.beer.data.services.BeerService
import com.davidups.beer.features.beer.domain.repository.BeerRepository
import com.davidups.beer.features.beer.domain.repository.BeerRepositoryImp
import com.davidups.beer.features.beer.domain.usescases.GetBeers
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.amshove.kluent.`should be instance of`
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner.Silent::class)
class GetBeersTest : UnitTest() {

    private lateinit var networkHandler: NetworkHandler
    private lateinit var repository: BeerRepository
    private lateinit var getBeers: GetBeers
    private val expectedBeers= mutableListOf<BeerEntity>()

    @Before
    fun setUp() {
        networkHandler = mock {
            onBlocking { isConnected } doReturn true
        }
    }

    @Test
    fun `should get beer on success`() = runBlocking {

        val service = mock<BeerService> {
            onBlocking { getBeer() } doReturn Response.success(expectedBeers)
        }

        repository =
            BeerRepositoryImp(networkHandler, service)

        getBeers = GetBeers(repository)

        val flow = getBeers.run()
        flow.collect { result ->
            result.`should be instance of`<Success<MutableList<BeerView>>>()
            when (result) {
                is Success<MutableList<BeerView>> -> {
                    result.data shouldBeEqualTo expectedBeers.map {
                        it.toData().toView()
                    }
                }
            }
        }
    }
}
