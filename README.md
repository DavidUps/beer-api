# Fintonic's Beers

## Screenshots
<img src="/assets/application.gif" width="260">


## Info
- MVVM arquitecture
- Unit Testing
- Android Jetpack.

## Fastlane
- Check Unit Testing.

## SonarQube
For user SonarQube you need to have a local server and put the next command

`sonarqube -Dsonar.login=key`

<img src="/assets/sonarQube.png" width="260">
